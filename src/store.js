import Vue from 'vue'
import VueX from 'vuex'

Vue.use(VueX)

export default new VueX.Store({
  state: {
    items: []
  },
  mutations: {
    addItem (state, item) {
      state.items.push(item)
    },
    initItems (state, items) {
      state.items = items
    }
  }
  // actions: {
  // not use actions... because this vuex has just one mutation
  // 1 mutation should use for 1 mutate
  // }
})
